wiki:  https://wiki.debian.org/deepanshugajbhiye
work-product-script:  get_latest.sh
work-product:  virt-ltsp-bionic.zip
work-product:  virt-ltsp-buster.zip
Detailed final report: https://gist.github.com/d78ui98/138c986dffc4d7a094e3ec1c63b545ba
Email:  gajbhiye.deepanshu@gmail.com
IRCnick:  deepanshuG
Repo:  https://github.com/docker-scripts/dev--LTSP
Docs:  https://github.com/docker-scripts/dev--LTSP/blob/bionic/README.md  
Docs:  https://github.com/docker-scripts/dev--LTSP/blob/buster/README.md
Docs:  https://github.com/docker-scripts/dev--LTSP/wiki
Project board:  https://github.com/docker-scripts/dev--LTSP/projects/1
Blog:  https://medium.com/@gajbhiyedeepanshu
Report1:  https://lists.debian.org/debian-outreach/2018/05/msg00020.html
Report2:  https://lists.debian.org/debian-outreach/2018/05/msg00050.html
Report3:  https://lists.debian.org/debian-outreach/2018/06/msg00015.html
Report4:  https://lists.debian.org/debian-outreach/2018/06/msg00056.html
Report5:  https://lists.debian.org/debian-outreach/2018/06/msg00086.html
Report6:  https://lists.debian.org/debian-outreach/2018/06/msg00120.html
Report7:  https://lists.debian.org/debian-outreach/2018/07/msg00000.html
Report8:  https://lists.debian.org/debian-outreach/2018/07/msg00034.html
Report9:  https://lists.debian.org/debian-outreach/2018/07/msg00059.html
Report10: https://lists.debian.org/debian-outreach/2018/07/msg00081.html
Report11: https://lists.debian.org/debian-outreach/2018/07/msg00107.html
Report12: https://lists.debian.org/debian-outreach/2018/08/msg00018.html
