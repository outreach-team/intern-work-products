#!/bin/bash

wget -O commits.json https://api.github.com/repos/docker-scripts/dev--LTSP/commits
wget -O virt-ltsp-buster.zip https://github.com/docker-scripts/dev--LTSP/archive/buster.zip
wget -O virt-ltsp-bionic.zip https://github.com/docker-scripts/dev--LTSP/archive/bionic.zip
wget -O final-report.md https://gist.githubusercontent.com/d78ui98/138c986dffc4d7a094e3ec1c63b545ba/raw
