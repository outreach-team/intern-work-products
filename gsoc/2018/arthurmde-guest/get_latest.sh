#!/bin/bash

wget -O distro-tracker.tar.gz https://salsa.debian.org/qa/distro-tracker/-/archive/master/distro-tracker-master.tar.gz
cwd=$(pwd)
cd /tmp/
git clone https://salsa.debian.org/qa/distro-tracker.git
cd /tmp/distro-tracker
git log --author="arthurmde@gmail.com" > $cwd/commits.txt
rm -rf /tmp/distro-tracker
