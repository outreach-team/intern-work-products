wiki: https://wiki.debian.org/aviau
work-product-script: generate-work-product.sh
work-product: work-product.tar.gz
email: alexandre@alexandreviau.net
IRCnick: aviau
repo (main code): https://salsa.debian.org/autodeb-team/autodeb
repo (infrastructure): https://salsa.debian.org/autodeb-team/infrastructure
repo (packaging): https://salsa.debian.org/autodeb-team/autodeb-packaging

report (week 1): https://lists.debian.org/debian-outreach/2018/05/msg00048.html
report (week 2): https://lists.debian.org/debian-outreach/2018/05/msg00049.html
report (week 3): https://lists.debian.org/debian-outreach/2018/06/msg00012.html
report (week 4): https://lists.debian.org/debian-outreach/2018/06/msg00052.html
report (week 5): https://lists.debian.org/debian-outreach/2018/06/msg00084.html
report (week 5&6): https://lists.debian.org/debian-outreach/2018/06/msg00131.html
report (week 7): https://lists.debian.org/debian-outreach/2018/07/msg00016.html
report (week 8): https://lists.debian.org/debian-outreach/2018/07/msg00017.html
report (week 9-12): https://lists.debian.org/debian-outreach/2018/08/msg00001.html
