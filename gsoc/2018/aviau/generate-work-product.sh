#!/usr/bin/env bash

set -e

rm -rf work-product
mkdir work-product

wget https://salsa.debian.org/autodeb-team/autodeb/-/archive/master/autodeb-master.tar.gz --output-document=work-product/autodeb.tar.gz
wget https://salsa.debian.org/autodeb-team/infrastructure/-/archive/master/infrastructure-master.tar.gz --output-document=work-product/infrastructure.tar.gz
wget https://salsa.debian.org/autodeb-team/autodeb-packaging/-/archive/master/autodeb-packaging-master.tar.gz --output-document=work-product/packaging.tar.gz
tar zcvf work-product.tar.gz work-product

rm -rf work-product
